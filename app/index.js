'use strict';

// Register the Babel require hook
require('babel-core/register');

// Export the generator
exports = module.exports = require('./generator');
